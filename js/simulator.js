/**
 * Copyright 2013 Friedolin Förder
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * @summary This script allows you to play your own radio stations
 * @description The script uses the HTML5 Audio class to play files
 * @version 1.0
 * @file simulator.js
 * @author Friedolin Förder <ff026@hdm-stuttgart.de>
 * @copyright Friedolin Förder 2013
 * @license Apache, Version 2.0 ({@link http://www.apache.org/licenses/LICENSE-2.0})
 */


var Station = (/** @lends Station */ function() {
    
    var constr,
        /**
         * @private
         *
         * @property {object} stations - A list of available stations 
         */
        stations = {},
        /**
         * @private
         *
         * @property {Audio} currentPlaying - The audio, which is currently playing
         */
        currentPlaying = null,
        /**
         * @private
         * 
         * @property {string} extension - The browser-specific extension of the
         *                                sound file
         */
        extension = "mp3";
    
    // wrap it in an try-catch block, so there is no error in safari 
    // without quicktime
    try {
        if(!document.createElement('audio').canPlayType("audio/mpeg")) {
            extension = "ogg";
        }
    } catch(error) {
        console.info("Audio not usable!");
    }
    
    
    /**
     * With this class, you can create your own radio stations. Therefore every station
     * uses a sound file and a unique radio station name.
     * <br /> 
     * The class uses the HTML5 Audio class to play files. It manages, that only
     * one station at a time is playing. Also when you switch to a station, the
     * sound file will not only resume, it calculates the new starting time and plays
     * from this time on.
     * <br />
     * You can also use {@link Station.add}
     *
     * @name Station
     * @constructor
     * 
     * @example
     * new Station("Radio Rock", "rockSong");
     * 
     * @param {string} name - The name of the radio station
     * @param {string} file - The file name without the extension
     */
    constr = function(name, file) {
        try {
            var audio = this.audio = new Audio(file+"."+extension);

            // set random start position
            audio.addEventListener('loadedmetadata', function() {
                this.currentTime = Math.random()*this.duration;
            });

            // loop the audio file
            if (typeof audio.loop == 'boolean') {
                audio.loop = true;
            } else {
                audio.addEventListener('ended', function() {
                    this.currentTime = 0;
                    this.play();
                }, false);
            }
            this.name = name;
            this.lastTimePlaying = null;
            stations[name] = this;
        } catch(error) {
            console.info("Playing of file "+file+"."+extension+" not possible!");
        }
    };
    
    /**
     * @lends Station.prototype
     */
    constr.prototype = {
        constructor: constr,
        /**
         * Play the radio station
         * 
         * @memberOf Station.prototype
         * 
         * @example
         * station.play();
         * 
         * @return {Station} Returns the current playing station (fluent interface)
         */
        play: function() {
            if(currentPlaying) {
                if(currentPlaying === this) {
                    return true;
                } else {
                    currentPlaying.pause();
                }
            }
            currentPlaying = this;

            var time = 0,
                now = +new Date();
            
            // calculate the new time for playing
            if(this.lastTimePlaying && this.audio.duration) {
                var diff = (now - this.lastTimePlaying)/1000;
                time = (this.audio.currentTime + diff) % this.audio.duration;
                this.audio.currentTime = time;
            }

            // play the audio snippet
            this.audio.play();
            
            // fluent interface
            return this;
        },
        /**
         * Pause the radio station
         * 
         * @memberOf Station.prototype
         * 
         * @example
         * station.pause();
         * 
         * @return {Station} Returns the current playing station (fluent interface)
         */
        pause: function() {
            if(currentPlaying === this) {
                this.audio.pause();
                this.lastTimePlaying = +new Date();
                currentPlaying = null;
            }
            
            // fluent interface
            return this;
        }
    };
    
    /**
     * Add a radio station
     * 
     * @static
     * @function
     * @name Station.add
     * 
     * @param {string} name - The name of the radio station
     * @param {string} file - The file name without the extension
     * 
     * @example
     * Station.add("Radio Rock", "rockSong");
     * 
     * @return {Station} Returns the Station class (fluent interface)
     */
    constr.add = function(name, file) {
        new Station(name, file);
        
        // fluent interface
        return this;
    };
    /**
     * Get a radio station by name
     *
     * @static
     * @function
     * @name Station.get
     * 
     * @param {string} name - The name of the radio station
     * 
     * @example
     * Station.get("Radio Rock");
     * 
     * @return {Station} Returns the Station instance
     */
    constr.get = function(name) {
        return stations[name];
    }
    /**
     * Play a radio station by name
     * 
     * @static
     * @public
     * @function
     * @name Station.play
     * 
     * @param {string} name - The name of the radio station
     * 
     * @example
     * Station.play("Radio Rock");
     * 
     * @return {Station} Returns the Station class (fluent interface)
     */
    constr.play = function(name) {
        var station = constr.get(name);
        if(station === undefined) {
            if(currentPlaying) {
                currentPlaying.pause();
            }
        } else {
            station.play();
        }
        
        // fluent interface
        return this;
    };
    /**
     * Loop over all stations
     * 
     * @static
     * @public
     * @function
     * @name Station.each
     * 
     * @param {string} callback - A callback function, which will be called for
     *                            every station
     * 
     * @return {Station} Returns the Station class (fluent interface)
     */
    constr.each = function(callback) {
        for(var i in stations) {
            callback.call(stations[i]);
        }
        
        // fluent interface
        return this;
    };

    return constr;
}());


/**
 * A radio simulation class
 * <br />
 * This class is an extension of the Station class. You can set the properties
 * of the radio, like e.g. the volume.
 *
 * @constructor
 * 
 * @param {object} options          - An object with options
 * @param {int}    options.volume   - The volume of the Radio
 * @param {object} options.stations - An object consisting of radio station names
 *                                    as keys and sound file names as values
 *                                    
 * @example
 * var simulator = new RadioSimulator({
 *     stations: {
 *         "Radio Rock": "rockSong",
 *         "Radio Pop":  "popSong"
 *     },
 *     mute: true
 * });
 */
var RadioSimulator = function(options) {
    var stations = options.stations;
    if(typeof stations === "object") {
        for(var name in stations) {
            Station.add(name, stations[name]);
        }
    }
    this._mute = !!options.mute;
};

RadioSimulator.prototype = {
    constructor: RadioSimulator,
    /**
     * Set the volume of the radio
     *
     * @param {int} value - The volume of the radio
     * 
     * @example
     * // set the volume to 30%
     * simulator.volume(0.3)
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface) 
     */
    volume: function(value) {
        this._volume = value;
        value = this._mute ? 0 : value;
        Station.each(function() {
            this.audio.volume = value;
        });
        
        // fluent interface
        return this;
    },
    /**
     * Mute the radio
     * 
     * @example
     * simulator.mute();
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface) 
     */
    mute: function() {
        this._mute = true;
        Station.each(function() {
            this.audio.volume = 0;
        });
        
        // fluent interface
        return this;
    },
    /**
     * Unmute the radio
     * 
     * @example
     * simulator.unmute();
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface) 
     */
    unmute: function() {
        this._mute = false;
        this.volume(this._volume);
        
        // fluent interface
        return this;
    },
    /**
     * Play a radio station by name
     * 
     * @param {string} name - The name of the radio station
     * 
     * @example
     * simulator.play("Radio Rock");
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface)
     */
    play: function(name) {
        if(!this._inactive) {
            Station.play(name);
        }
        
        // fluent interface
        return this;
    },
    /**
     * Activate the radio
     * 
     * @example
     * simulator.activate();
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface)
     */
    activate: function() {
        this._inactive = false;
        
        // fluent interface
        return this;
    },
    /**
     * Deactivate the radio
     * 
     * @example
     * simulator.deactivate();
     * 
     * @return {RadioSimulator} Returns the RadioSimulator class (fluent interface)
     */
    deactivate: function() {
        this._inactive = true;
        Station.play("");
        
        // fluent interface
        return this;
    }
}