/**
 * Copyright 2013 Friedolin Förder
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * @summary URC Radio Script
 * @description Implementation of a universal remote console of a radio
 * @version 1.0
 * @file script.js
 * @author Friedolin Förder <ff026@hdm-stuttgart.de>
 * @overview This is a implementation of the Univeral Remote Console (URC) 
 * for a radio device. With this application you can control the
 * the main functions of the radio (like setting the volume, balance, bass,
 * treble and much more).
 * <br />
 * The values of the radio come from the UCH (Universal Control Hub) and
 * changes will be sent to the UCH, too.
 * <br /><br />
 * This script uses jQuery and the urc plugin for jQuery (jquery.urc.js, see the 
 * <a href="../jqueryurc/docu/index.html">plugin dokumentation page</a>)
 * @copyright Friedolin Förder 2013
 * @license Apache, Version 2.0 ({@link http://www.apache.org/licenses/LICENSE-2.0})
 * @requires jQuery
 * @requires jQuery.urc
 */

jQuery(/** @lends <global> */function($) {
    
    /**
     * @private
     * 
     * @property {object} data - The default data
     */
    var data = {
        isOn: false,
        isMuted: false,
        sleepTimerIsActive: false,
        reminderIsActive: false,
        signalStrength: null,
        volume: null,
        treble: null,
        bass: null,
        balance: null,
        frequencyBandRanges: {
            LF: {
                bandMin: 300,
                bandMax: 300000
            },
            MF: {
                bandMin: 300000,
                bandMax: 3000000
            },
            HF: {
                bandMin: 3000000,
                bandMax: 300000000
            }
        },
        frequencies: {
            LF: {
                frequencyBandColumn: 30000
            },
            MF: {
                frequencyBandColumn: 1000000
            },
            HF: {
                frequencyBandColumn: 100000000
            }
        },
        nameOfStation: null,
        favoriteStations: {},
        availableStations: {},
        stations: {
            LF: [],
            MF: [],
            HF: []
        },
        reminderMinutes: null,
        sleepTimerMinutes: null
    };
    
    /**
     * An instance of the RadioSimulator.
     * <br />
     * With this instance the radio will be simulated.
     *
     * @instance
     */
    var simulator = new RadioSimulator({
        stations: {
            "Hitradio N1":         "music/20110214",
            "Energy Stuttgart":    "music/Columbia_Orchestra-Smoky_Mokes-1902",
            "Radio BW":            "music/DownhillDrag",
            "Underground Radio":   "music/InternationalNoveltyOrchestrawithBillyMurray-MonkeyBiz-nessDowninTennessee1925a",
            "Classic MF":          "music/MaddoxBrothersAndRose-MuleTrain1949",
            "Slam Jam MF":         "music/StorySlam2.13-GettingAround-07TheLostBirthdayParty"
        },
        volume: 1
    }).deactivate();
    
    /**
     * @description Helper functions
     * 
     * @private
     * 
     * @namespace
     */
    var Urc = {
        /**
         * Create an object out of a bunch of arguments.
         * 
         * @param {object} [obj] unlimited OPTIONAL number of objects
         * 
         * @return {object} The object that was created out of the arguments
         */
        args2obj: function() {
            var obj = {},
                current = obj,
                i = 0,
                length = arguments.length;

            while(i < length - 2) {
                current[arguments[i]] = {};
                current = current[arguments[i]];
                i++;
            }
            current[arguments[i]] = arguments[i+1];
            return obj;
        }
    };
        
        /**
         * @private
         * 
         * @property {int} markerCount - The number of markers for the frequency display
         */
    var markerCount = 50,
        /**
         * @private
         * 
         * @property {int} transitionSpeed - The speed for the transitions
         */
        transitionSpeed = 300,
        /**
         * @private
         * 
         * @property {object} $focusItem - The current focused item
         */
        $focusItem = null,
        /**
         * An event function to determine the current focused item.
         * 
         * @private
         */
        focusOnItem = function(event) {
            $focusItem = $(this);
            console.log($focusItem);
            
            $focusItem.one("blur", function() {
                $focusItem = null;
            });
            return false;
        };
    
    
    ////////////////////////////////////////////////////////////////////////////////
    // START OF jQuery/jQueryUI CONFIGURATION:                                    //
    //   - add dialogs for notifications                                          //
    //   - open dialogs with buttons                                              //
    //   - add EventHandlers for Power-Button                                     //
    //   - add EventHandlers for Sleep-Button                                     //
    //   - add EventHandlers for Reminder-Button                                  //
    //   - add EventHandlers for Mute-Button                                      //
    //   - add EventHandlers for Station-Button                                   //
    //   - add Buttonset to frequencyBand-Inputs                                  //
    //   - add Sliders for volume, treble, bass, balance, sleepTimer              //
    //     and ReminderTimer                                                      //
    //   - add EventHandlers for the to-left/to-right frequency buttons           //
    //   - add EventHandlers for the previous/next frequency buttons              //
    //   - add tabs for the two stations lists                                    //
    //   - initialize the markers for the frequency display                       //
    //   - add Keyboard-Support for the buttons (click with enter)                //
    //   - add EventHandlers for the list-items                                   //
    //   - disable all sliders and buttons                                        //
    ////////////////////////////////////////////////////////////////////////////////
    
    
    // 
    // add dialogs for notifications
    //
    $("#dialog-confirmClearing").dialog({
        autoOpen: false,
        width: 500,
        resizable: false,
        modal: true,
        buttons: {
            "Clear all items": function() {
                set("confirmClearing", "inactive");
                $(this).dialog("close");
            },
            Cancel: function() {
                set("confirmClearing", "inactive");
                $(this).dialog("close");
            }
        }
    });
    
    $("#dialog-confirmUpdate").dialog({
        autoOpen: false,
        width: 500,
        resizable: false,
        modal: true,
        buttons: {
            "Search stations": function() {
                set("confirmUpdate", "inactive");
                $(this).dialog("close");
            },
            Cancel: function() {
                set("confirmUpdate", "inactive");
                $(this).dialog("close");
            }
        }
    });
    
    // 
    // open dialogs with buttons
    //
    $("#list .buttons .clear").click(function() {
        set("confirmClearing", "active");    
    });
    
    $("#list .buttons .search").click(function() {
        set("confirmUpdate", "active");    
    });
    
    // 
    // add EventHandlers for Power-Button
    //
    $("#powerButton a")
        .click(function() {
            return false;
        })
        .mousedown(function() {
            $(this).parent().addClass("down");
        })
        .mouseup(function() {
            set("isOn", !data.isOn);
            $(this).parent()
                .removeClass("down");
        })
        .focus(focusOnItem);
        
    // 
    // add EventHandlers for Sleep-Button
    //
    $("#sleepButton a")
        .click(function() {
            return false;
        })
        .mousedown(function() {
            $(this).parent().addClass("down");
        })
        .mouseup(function() {
            if(data.isOn) {
                set("sleepTimerIsActive", !data.sleepTimerIsActive);
            }
            $(this).parent()
                .removeClass("down");
        })
        .focus(focusOnItem);
        
    // 
    // add EventHandlers for Reminder-Button
    //
    $("#reminderButton a")
        .click(function() {
            return false;
        })
        .mousedown(function() {
            $(this).parent().addClass("down");
        })
        .mouseup(function() {
            if(data.isOn) {
                set("reminderIsActive", !data.reminderIsActive);
            }
            $(this).parent()
                .removeClass("down");
        })
        .focus(focusOnItem);
        
    // 
    // add EventHandlers for Mute-Button
    //
    $("#muteButton a")
        .click(function() {
            return false;
        })
        .mousedown(function() {
            $(this).parent().addClass("down");
        })
        .mouseup(function() {
            if(data.isOn) {
                set("isMuted", !data.isMuted);
            }
            $(this).parent()
                .removeClass("down");
        })
        .focus(focusOnItem);
        
    // 
    // add Slider for frequency
    //
    $("#frequency .slider").slider({
        animate: "fast",
        slide: function(event, ui) {
            var band = getBand(ui.value);
            set("frequencies", Urc.args2obj(band, "frequencyBandColumn", ui.value));
        }
    });
    
    // 
    // add EventHandlers for Station-Button
    //
    $("#frequency .station .button")
        .button()
        .click(function() {
            if(!data.isOn) return false;
            
            var frequency = data.frequencies[data.frequencyBand].frequencyBandColumn;
            if(data.favoriteStations[frequency] === undefined) {
                $(this).children().text("remove");
                addFavorite(frequency);
            } else {
                $(this).children().text("add");
                removeFavorite(frequency);
            }
        })
        .children()
            .text("add");
    
    //
    // add Buttonset to frequencyBand-Inputs
    //
    $("#frequency .band")
        .buttonset()
        .children("input")
            .click(function(event) {
                if(!data.isOn) {
                    event.preventDefault();
                    return false;
                }
                
                set("frequencyBand", $(this).attr("id"));
            });
    
    //
    // add EventHandlers for to-left/to-right frequency buttons
    //
    $("#frequency .toLeft")
        .click(function() {
            var stations = data.stations[data.frequencyBand],
                stationsLength = stations.length,
                frequency = data.frequencies[data.frequencyBand].frequencyBandColumn,
                index = stations.indexOf(frequency),
                newIndex = stationsLength-1;
            
            if(stationsLength < 1 || !data.isOn) return;
            
            if(index != -1) {
                if(index > 0) {
                    newIndex = index-1;
                } else {
                    newIndex = stationsLength-1;
                }
            } else {
                for(var i = 0; i<stationsLength; i++) {
                    if(stations[i] < frequency) {
                        newIndex = i;
                    } else {
                        break;
                    }
                }
            }
            set("frequencies", Urc.args2obj(
                data.frequencyBand, 
                "frequencyBandColumn", 
                stations[newIndex]
            ));
        })
        .focus(focusOnItem);
    
    $("#frequency .toRight")
        .click(function() {
            var stations = data.stations[data.frequencyBand],
                stationsLength = stations.length,
                frequency = data.frequencies[data.frequencyBand].frequencyBandColumn,
                index = stations.indexOf(frequency),
                newIndex = 0;
                
            if(stationsLength < 1 || !data.isOn) return;
                
            if(index != -1) {
                if(index < stations.length-1) {
                    newIndex = index+1;
                } else {
                    newIndex = 0;
                }
            } else {
                for(var i = stationsLength; i--; ) {
                    if(stations[i] > frequency) {
                        newIndex = i;
                    } else {
                        break;
                    }
                }
            }
            
            set("frequencies", Urc.args2obj(
                data.frequencyBand, 
                "frequencyBandColumn", 
                stations[newIndex]
            ));
        })
        .focus(focusOnItem);
    
    //
    // add EventHandlers for previous/next frequency buttons
    //
    $("#frequency .minus")
        .click(function() {
            var step = $("#frequency .slider").slider("option", "step"),
                freq = data.frequencies[data.frequencyBand].frequencyBandColumn,
                frequency = freq - step;
                
            if(!data.isOn) return;
            
            if(frequency < data.frequencyBandRanges[data.frequencyBand].bandMin) {
                frequency = data.frequencyBandRanges[data.frequencyBand].bandMax;
            }
            set("frequencies", Urc.args2obj(
                data.frequencyBand, 
                "frequencyBandColumn", 
                frequency
            ));
        })
        .focus(focusOnItem);
        
    $("#frequency .plus")
        .click(function() {
            var step = $("#frequency .slider").slider("option", "step"),
                freq = data.frequencies[data.frequencyBand].frequencyBandColumn,
                frequency = freq + step;
                
            if(!data.isOn) return;
            
            if(frequency > data.frequencyBandRanges[data.frequencyBand].bandMax) {
                frequency = data.frequencyBandRanges[data.frequencyBand].bandMin;
            }
            set("frequencies", Urc.args2obj(
                data.frequencyBand, 
                "frequencyBandColumn", 
                frequency
            ));
        })
        .focus(focusOnItem);
    
    //
    // add tabs for the two stations lists    
    //
    $("#list .tabs").tabs();
    
    //
    // change to button style
    //
    $("#list .buttons input")
        .button()
        .disableSelection();
    
    // 
    // initialize the markers for the frequency display
    //
    var $markerItems = $([]);
    for(var i = 0, length = markerCount; i <= length; i++ ) {
        var left = (i/length*100),
            $markerItem = $("<div class='line'></div>").css("left", left + "%");
        if(i%5 == 0) {
            $markerItems = $markerItems
                .add($("<div class='label'></div>")
                .css("left", left + "%")
                .text(i));
            $markerItem.addClass("big");
        }
        $markerItems = $markerItems.add($markerItem);
    }
    $("#frequency .marker .inner").append($markerItems);
    
    // 
    // add Slider for volume
    //
    $("#volume .slider")
        .slider({
            min: 0,
            max: 100,
            step: 1,
            orientation: "vertical",
            animate: "fast",
            range: "min",
            slide: function(event, ui) {
                set("volume", ui.value, false);
            }
        });
    
    // 
    // add Slider for treble
    //
    $("#sound .trebleSlider .slider")
        .slider({
            min: -12,
            max: 12,
            step: 1,
            orientation: "vertical",
            animate: "fast",
            range: "max",
            slide: function(event, ui) {
                set("treble", ui.value, false);
            }
        });
    
    //
    // add Slider for bass
    //
    $("#sound .bassSlider .slider")
        .slider({
            min: -12,
            max: 12,
            step: 1,
            orientation: "vertical",
            animate: "fast",
            range: "max",
            slide: function(event, ui) {
                set("bass", ui.value, false);
            }
        });
    
    // 
    // add Slider for balance
    //
    $("#sound .balanceSlider .slider")
        .slider({
            min: -50,
            max: 50,
            step: 1,
            animate: "fast",
            range: "max",
            slide: function(event, ui) {
                set("balance", ui.value, false);
            }
        });
    
    //
    // add Slider for sleepTimer
    //
    $("#sleepTimer .slider")
        .slider({
            min: 0,
            max: 240,
            step: 1,
            animate: "fast",
            range: "max",
            slide: function(event, ui) {
                set("sleepTimerMinutes", 240 - ui.value, false);
            }
        });
    
    // 
    // add Slider for reminderTimer
    //
    $("#reminderTimer .slider")
        .slider({
            min: 0,
            max: 240,
            step: 1,
            animate: "fast",
            range: "max",
            slide: function(event, ui) {
                set("reminderMinutes", 240 - ui.value, false);
            }
        });

    //
    // add EventHandlers for the list-items
    //
    $("#favoriteStations ul, #allStations ul")
        .on("mouseenter", ".ui-button", function() {
            $(this).parent().parent().addClass("buttonHovered");
        })
        .on("mouseleave", ".ui-button", function() {
            $(this).parent().parent().removeClass("buttonHovered");
        })
        .on("click", "li", function() {
            var frequency = parseInt($(this).attr("data-frequency"), 10);
            set("frequencyBand", getBand(frequency));
            set("frequencies", Urc.args2obj(
                data.frequencyBand,
                "frequencyBandColumn", 
                frequency
            ))
        })
        .on("focus", "li, .button", focusOnItem);
    
    // 
    // add Keyboard-Support for the buttons (click with enter)
    //
    $("body")
        .on("keydown", function(event) {
            if($focusItem) {
                if(event.which == 13) {
                    var $parent = $focusItem.parent();
                    if($parent.is("#powerButton")) {
                        set("isOn", !data.isOn);
                    } else if($parent.is("#muteButton")) {
                        set("isMuted", !data.isMuted);
                    } else if($parent.is("#sleepButton")) {
                        set("sleepTimerIsActive", !data.sleepTimerIsActive);
                    } else if($parent.is("#reminderButton")) {
                        set("reminderIsActive", !data.reminderIsActive);
                    } else {
                        $focusItem.click().focus();
                    }
                } else if(event.which == 8 || event.which == 46) {
                    var frequency = parseInt($focusItem.attr("data-frequency"), 10);
                    if(data.favoriteStations[frequency] !== undefined) {
                        $focusItem.find(".ui-button span").text("add");
                        removeFavorite(frequency);
                    }
                    removeFavorite(parseInt($focusItem.attr("data-frequency"), 10));
                } else if(event.which == 45) {
                    var frequency = parseInt($focusItem.attr("data-frequency"), 10);
                    if(data.favoriteStations[frequency] === undefined) {
                        $focusItem.find(".ui-button span").text("remove");
                        addFavorite(frequency);
                    }
                }
            } 
        })
        .disableSelection();
        
    //
    // disable all sliders and buttons    
    //
    $(".slider").slider("disable");
    $(".button, .ui-buttonset input, .buttons input").button("disable");
    
    ////////////////////////////////////////////////////////////////////////////////
    // END OF jQuery/jQueryUI CONFIGURATION                                       //
    ////////////////////////////////////////////////////////////////////////////////
    
    /**
     * @private
     * @static
     * 
     * @param {boolean} [dirty=true]
     * @param {string}  key
     * @param {object}  value
     * 
     * @example
     * set("isOn", true)
     * @example
     * set(false, "isOn", true)
     */
    var set = function(key, value) {
            var dirty = true,
                values = Array.prototype.slice.call(arguments, 1);
                
            if(typeof key === "boolean") {
                dirty = key;
                key = value;
                values.shift();
            }
            
            if(dirty !== false) {
                $.urc("set", key, value);
            }
            // call the setter function
            if(setter[key] instanceof Function) {
                setter[key].apply(this, $.extend(true, [], values));
            }
        },
        /**
         * @description Setter functions for all remote variables, 
         *              notifications and commands.
         * 
         * @private
         * @static
         * 
         * @namespace setter
         */
        setter = {
            /**
             * Confirm the user about clearing all favorite stations and close 
             * or open the dialog.
             * 
             * @function
             * @static
             * 
             * @param {string} value - The current state (active or inactive)
             */
            confirmClearing: (function() {
                var $dialog = $("#dialog-confirmClearing");

                return function(value) {
                    if(value == "inactive") {
                        $dialog.dialog("close");
                    } else {
                        $dialog.dialog("open");
                    }
                };
            }()),
            /**
             * Confirm the user about updating all available stations and close
             * or open the dialog.
             * 
             * @function
             * @static
             * 
             * @param {string} value - The current state (active or inactive)
             */
            confirmUpdate: (function() {
                var $dialog = $("#dialog-confirmUpdate");

                return function(value) {
                    if(value == "inactive") {
                        $dialog.dialog("close");
                    } else {
                        $dialog.dialog("open");
                    }
                };
            }()),
            /**
             * Turn the radio on or off
             * 
             * @function
             * @static
             * 
             * @param {boolean} isOn - True if the radio is on, otherwise false
             */
            isOn: (function() {
                var $body = $("body"),
                    $slider = $(".slider"),
                    $buttons = $(".button, .ui-buttonset input, .buttons input"),
                    $stations = $("#favoriteStations ul, #allStations ul");

                return function(isOn) {
                    if(!isOn || data.isMuted) {
                        simulator.mute();
                    } else {
                        simulator.unmute();
                    }

                    if(isOn) {
                        $slider.slider("enable");
                        $stations.find(".button").add($buttons).button("enable");
                        $body.addClass("on");
                    } else {
                        $slider.slider("disable");
                        $stations.find(".button").add($buttons).button("disable");
                        $body.removeClass("on");
                    }
                };
            }()),
            /**
             * Mute or unmute the radio
             * 
             * @function
             * @static
             * 
             * @param {boolean} isMuted - True if the radio is muted, otherwise
             *                            false
             */
            isMuted: (function() {
                var $volume = $("#volume");

                return function(isMuted) {
                    if(!data.isOn || isMuted) {
                        simulator.mute();
                    } else {
                        simulator.unmute();
                    }
                    
                    if(isMuted) {
                        // mute simulator
                        simulator.mute();
                        // change style (to green)
                        $volume.addClass("on");
                    } else {
                        // change style (to red)
                        $volume.removeClass("on");
                    }
                };
            }()),
            /**
             * Activate or deactivate the sleepTimer
             * 
             * @function
             * @static
             * 
             * @param {boolean} sleepTimerIsActive - True for activating the
             *                                       sleepTimer, otherwise
             *                                       false
             */
            sleepTimerIsActive: (function() {
                var $sleepButton = $("#sleepTimer");

                return function(sleepTimerIsActive) {
                    data.sleepTimerIsActive = sleepTimerIsActive;

                    if(sleepTimerIsActive) {
                        $sleepButton.addClass("on");
                    } else {
                        $sleepButton.removeClass("on");
                    }
                };
            }()),
            /**
             * Activate or deactivate the reminderTimer
             *
             * @function
             * @static
             * 
             * @param {boolean} reminderIsActive - True for activating the
             *                                     reminderTimer, otherwise
             *                                     false
             */
            reminderIsActive: (function() {
                var $reminderButton = $("#reminderTimer");

                return function(reminderIsActive) {
                    data.reminderIsActive = reminderIsActive;

                    if(reminderIsActive) {
                        $reminderButton.addClass("on");
                    } else {
                        $reminderButton.removeClass("on");
                    }
                };
            }()),
            /**
             * The strength of the radio signal
             * 
             * @function
             * @static
             * 
             * @param {number} signalStrength - The strength in percent
             */
            signalStrength: (function() {
                var $strengthInner = $("#strength .inner");

                return function(signalStrength) {
                    $strengthInner.css("height", (100 - Math.round(signalStrength/10)*10) + "%");
                };
            }()),
            /**
             * Set the frequencies
             *
             * @static
             * 
             * @param {Object}  frequencies         - An object with frequencies
             *                                        for the specific frequency
             *                                        bands
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            frequencies: (function() {
                var $slider = $("#frequency .slider"),
                    $frequency = $("#frequency .frequency .value, #header .frequency");
                
                return function(frequencies, updateSlider) {
                    var frequency;
                    for(var band in frequencies) {
                        frequency = frequencies[band].frequencyBandColumn;
                    }
                    
                    if(updateSlider !== false) {
                        $slider.slider("value", frequency);
                    }

                    $frequency.text(getFormatedFrequency(frequency));
                    
                    findAndUpdateCurrentStation(frequency);

                    
                }
            }()),
            /**
             * Set the current frequency band
             * 
             * @function
             * @static
             * 
             * @param {String} frequencyBand - The frequency band
             */
            frequencyBand: (function() {
                var $slider = $("#frequency .slider"),
                    $bandButtonset = $("#frequency .band"),
                    $button = $("#frequency .band"),
                    $band = $("#header .band");

                return function(frequencyBand) {
                    var min = data.frequencyBandRanges[frequencyBand].bandMin,
                        max = data.frequencyBandRanges[frequencyBand].bandMax,
                        diff = max - min,
                        step;

                    switch(frequencyBand) {
                        case "LF": step = 10; break;
                        case "MF": step = 1000; break;
                        case "HF": step = 50000; break;
                    }

                    $slider
                        .slider("option", "min", min)
                        .slider("option", "max", max)
                        .slider("option", "value", data.frequencies[frequencyBand]);
                    
                    // set frequency band ranges
                    setter.frequencyBandRanges(Urc.args2obj(
                        frequencyBand, 
                        data.frequencyBandRanges[frequencyBand]
                    ));

                    $slider
                        .slider("option", "step", step);

                    $button.children("#"+frequencyBand).attr("checked", "checked");

                    $band.text(frequencyBand);
                    $bandButtonset.children("[for='"+frequencyBand+"']").click();
                    
                    // set frequency
                    set(false, "frequencies", Urc.args2obj(
                        frequencyBand, 
                        data.frequencies[frequencyBand]
                    ));
                };
            }()),
            /**
             * Set the ranges of the frequency bands
             * 
             * @function
             * @static
             * 
             * @param {object} ranges - The ranges of the frequency bands
             */
            frequencyBandRanges: (function() {
                var $markerInner = $("#frequency .marker .inner");

                return function(ranges) {
                    if(data.frequencyBand == null) return; 

                    // get last range
                    var range;
                    for(var i in ranges) {
                        range = ranges[i];
                    }
                    // find best interval
                    var min = range.bandMin,
                        max = range.bandMax,
                        diff = max - min,
                        section = diff/7,
                        lastVarianz = 0,
                        varianz = 100000000,
                        interval = 0.5;

                    while(true) {
                        lastVarianz = varianz;
                        varianz = Math.abs(section - interval);
                        if(varianz >= lastVarianz) {
                            interval /= 10;
                            break;
                        }
                        interval *= 10;
                    } 

                    var start = min - (min%interval),
                        startDiff = start - min,
                        startDiffInProcent = startDiff/diff*100;
                    
                    // configure marker
                    $markerInner
                        .css({
                            left: startDiffInProcent.toFixed(4) + "%",
                            width: (10/(diff/interval)*100).toFixed(4) + "%"
                        })
                        .children(".label")
                            .each(function(index) {
                                $(this).text(getFormatedFrequency(start + index*interval, true));
                            });
                };
            }()),
            /**
             * Set the name of the current playing radio station
             * 
             * @function
             * @static
             * 
             * @param {string} nameOfStation - The name of the current playing
             *                                 radio station
             */
            nameOfStation: (function() {
                var $name = $("#frequency .station .value, #header .station");

                return function(nameOfStation) {
                    data.nameOfStation = nameOfStation;

                    $name.text(nameOfStation);
                };
            }()),
            /**
             * Set the volume of the radio
             * 
             * @function
             * @static
             * 
             * @param {int}     volume              - The volume of the radio
             *                                        in percent
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            volume: (function() {
                var $slider = $("#volume .slider"),
                    $label = $slider.next();

                return function(volume, updateSlider) {
                    data.volume = volume;

                    if(updateSlider !== false) {
                        $slider.slider("value", volume);
                    }
                    $label
                        .text(volume + "%")
                        .css("bottom", volume + "%");
                        
                    // set the volume of the simulator    
                    simulator.volume(volume/100);
                };
            }()),
            /**
             * Set the value for the treble property of the radio
             * 
             * @function
             * @static
             * 
             * @param {int}     treble              - The treble property value
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            treble: (function() {
                var $slider = $("#sound .trebleSlider .slider"),
                    $range = $slider.children(".ui-slider-range-max"),
                    $label = $slider.next();

                return function(treble, updateSlider) {
                    data.treble = treble;

                    var pos = (50 + treble/12*50);

                    if(updateSlider !== false) {
                        $slider.slider("value", treble);
                    }
                    if(treble > 0) {
                        $range.css({
                            top: (100 - pos) + "%",
                            bottom: "50%"
                        });
                    } else {
                        $range.css({
                            top: "50%",
                            bottom: pos + "%"
                        });
                    }
                    $label
                        .text(treble + "dB")
                        .css("bottom", pos + "%");
                }; 
            }()),
            /**
             * Set the value for the bass property of the radio
             *
             * @function
             * @static
             * 
             * @param {int}     bass                - The bass property value
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            bass: (function() {
                var $slider = $("#sound .bassSlider .slider"),
                    $range = $slider.children(".ui-slider-range-max"),
                    $label = $slider.next();

                return function(bass, updateSlider) {
                    data.bass = bass;

                    var pos = (50 + bass/12*50);

                    if(updateSlider !== false) {
                        $slider.slider("value", bass);
                    }
                    if(bass > 0) {
                        $range.css({
                            top: (100 - pos) + "%",
                            bottom: "50%"
                        });
                    } else {
                        $range.css({
                            top: "50%",
                            bottom: pos + "%"
                        });
                    }
                    $label
                        .text(bass + "dB")
                        .css("bottom", pos + "%");
                }; 
            }()),
            /**
             * Set the value for the balance property of the radio
             * 
             * @function
             * @static
             * 
             * @param {int}     balance             - The balance property value
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            balance: (function() {
                var $slider = $("#sound .balanceSlider .slider"),
                    $range = $slider.children(".ui-slider-range-max"),
                    $label = $slider.next();

                return function(balance, updateSlider) {
                    data.balance = balance;

                    var pos = (50 + balance/50*50);

                    if(updateSlider !== false) {
                        $slider.slider("value", balance);
                    }
                    if(balance > 0) {
                        $range.css({
                            left: "50%",
                            right: (100 - pos) + "%"
                        });
                    } else {
                        $range.css({
                            left: pos + "%",
                            right: "50%"
                        });
                    }
                    $label
                        .text(balance)
                        .css("left", pos + "%");
                }; 
            }()),
            /**
             * Set the remaining sleep timer minutes
             * 
             * @function
             * @static
             * 
             * @param {int}     sleepTimerMinutes   - The minutes till the
             *                                        sleep timer shuts down
             *                                        the radio
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            sleepTimerMinutes: (function() {
                var $slider = $("#sleepTimer .slider"),
                    $label = $slider.next();

                return function(sleepTimerMinutes, updateSlider) {
                    data.sleepTimerMinutes = sleepTimerMinutes;

                    var diff = 240-sleepTimerMinutes,
                        pos = (diff/240*100);

                    if(updateSlider !== false) {
                        $slider.slider("value", diff);
                    }

                    $label
                        .text(sleepTimerMinutes + " min.")
                        .css("left", pos + "%");
                };
            }()),
            /**
             * Set the remaining reminder minutes
             *
             * @function
             * @static
             * 
             * @param {int}     reminderMinutes     - The minutes till the
             *                                        reminder is shown 
             * @param {boolean} [updateSlider=true] - Set to false if the slider
             *                                        should not be updated,
             *                                        otherwise true (this is
             *                                        the default)
             */
            reminderMinutes: (function() {
                var $slider = $("#reminderTimer .slider"),
                    $label = $slider.next();

                return function(reminderMinutes, updateSlider) {
                    data.reminderMinutes = reminderMinutes;

                    var diff = 240-reminderMinutes,
                        pos = (diff/240*100);

                    if(updateSlider !== false) {
                        $slider.slider("value", diff);
                    }

                    $label
                        .text(reminderMinutes + " min.")
                        .css("left", pos + "%");
                };
            }()),
            /**
             * Set the favorite stations of the radio
             * 
             * @function
             * @static
             * 
             * @param {Object} favorites - An object which contains the favorite 
             *                             radios stations
             */
            favoriteStations: (function() {
                var stations = data.stations,
                    $favorites = $("#favoriteStations ul")
                        .on("click", ".ui-button", function() {
                            if(!data.isOn) return false;

                            var frequency = parseInt($(this).parent().parent().attr("data-frequency"), 10);
                            removeFavorite(frequency);
                            return false;
                        });

                return function(favorites) {
                    var $items = $([]),
                        toUpdate = {};
                        
                    for(var frequency in favorites) {
                        var station = favorites[frequency];
                        
                        // convert to int
                        frequency = parseInt(frequency, 10);
                        
                        $items = $items.add(createListItem(frequency, station.favoriteStationsBand, station.favoriteStationsNameOfStation));
                        var band = getBand(frequency);
                        stations[band].push(frequency);
                        toUpdate[band] = true;
                    }
                    for(var _band in toUpdate) {
                        // sort stations numerically
                        stations[_band].sort(function(a,b){return a-b});
                    }

                    // add new stations
                    $favorites.append($items);
                    
                    // set the current state
                    findAndUpdateCurrentStation(data.frequencies[data.frequencyBand].frequencyBandColumn);
                    
                    // highlight the list
                    highlightList();
                };
            }()),
            /**
             * Set the available stations of the radio
             * 
             * @function
             * @static
             * 
             * @param {Object} availables - An object which contains all radios 
             *                              stations
             */
            availableStations: (function() {
                var $all = $("#allStations ul")
                        .on("click", ".ui-button", function() {
                            if(!data.isOn) return false;
                            
                            // find frequency
                            var frequency = parseInt($(this).parent().parent().attr("data-frequency"), 10);
                            
                            if(data.favoriteStations[frequency] === undefined) {
                                $(this).children().text("remove");
                                addFavorite(frequency);
                            } else {
                                $(this).children().text("add");
                                removeFavorite(frequency);
                            }
                            return false;
                        });

                return function(availables) {

                    // DOM
                    var $items = $([]);
                    for(var band in availables) {
                        var stations = availables[band].availableStationsItem;
                        for(var frequency in stations) {
                            frequency = parseInt(frequency, 10);
                            $items = $items.add(createListItem(frequency, band, stations[frequency].availableStationsNameOfStation));
                        }
                    }

                    // add new stations
                    $all.append($items);

                    highlightList();
                };
            }())
        };
        /**
         * Finds the current station in the list, highlights it and plays it
         * 
         * @private
         * @function
         * 
         * @param {int} frequency - The new frequency
         * 
         * @return {jQuery} The buttons as jQuery object
         */
    var findAndUpdateCurrentStation = (function() {
            var $favoriteStations = $("#favoriteStations ul"),
                $allStations = $("#allStations ul"),
                $button = $("#frequency .button"),
                $buttonInner = $button.children("span");
            
            return function(frequency) {
                console.log(frequency);
                var band = getBand(frequency);
                    
                // find active station in lists
                if(data.availableStations[band] === undefined || data.availableStations[band].availableStationsItem[frequency] === undefined) {
                    $button.fadeOut(transitionSpeed);
                    set("nameOfStation", "Unknown Station");
                } else {
                    $button.fadeIn(transitionSpeed);
                    $buttonInner.text(data.favoriteStations[frequency] === undefined ? "add" : "remove");
                    set("nameOfStation", data.availableStations[band].availableStationsItem[frequency].availableStationsNameOfStation);
                }
                
                var $station = $favoriteStations.children().add($allStations.children())
                    .removeClass("active")
                    .filter("[data-frequency="+frequency+"]")
                        .addClass("active");
                        
                // play station
                simulator.play($station.first().find(".station .value").text());
                
                return $button;
            };
        }()),
        /**
         * Create a list item
         * 
         * @private
         * 
         * @param {int}
         * @param {String}
         * @param {String}
         * 
         * @return {jQuery} A jQuery object that represents the list item
         */
        createListItem = function(frequency, band, name) {
            var $button = $("<span class='button' tabindex='0'>remove</span>").button({disabled: !data.isOn});
            
            return $("<li data-frequency='"+frequency+"' tabindex='0'></li>")
                .append($("<div class='station'><span class='value'>"+name+"</span></div>").append($button))
                .append("<span class='frequency'>"+getFormatedFrequency(frequency)+", "+band+"</span>");
        },
        /**
         * Add a favorite to the list of favorite radio stations
         * 
         * @private
         * @function
         * 
         * @param {int} frequency
         * 
         * @return {jQuery} An jQuery object, which contains the child 
         *                  jQuery objects of the favorite list
         */
        addFavorite = (function() {
            var stations = data.stations,
                $allStations = $("#allStations ul"),
                $favoriteStations = $("#favoriteStations ul");
            
            return function(frequency) {
                var band = getBand(frequency);
                
                // DOM
                var $clone = $allStations.find("[data-frequency="+frequency+"]")
                    .clone(true)
                    .find(".ui-button span")
                        .text("remove")
                        .end();
                        
                var $children = $favoriteStations
                    .append($clone.hide().fadeIn(transitionSpeed))
                    .children()
                        .sort(function(a, b) {
                            return parseInt($(b).attr("data-frequency"), 10) < parseInt($(a).attr("data-frequency"), 10);
                        })
                        .appendTo($favoriteStations);
                
                $.urc("add", Urc.args2obj("favoriteStations", frequency, {
                    favoriteStationsBand: band,
                    favoriteStationsNameOfStation: $clone.find(".station .value").text()
                }));
                
                // update station index
                stations[band].push(frequency);
                // sort stations numerically
                stations[band].sort(function(a,b){return a-b});
                
                $allStations
                    .children("[data-frequency="+frequency+"]")
                        .find(".ui-button span")
                            .text("remove");
                
                highlightList();
                findAndUpdateCurrentStation(data.frequencies[data.frequencyBand].frequencyBandColumn);
                
                return $children;
            };
        }()),
        /**
         * Remove a radio station from the list of favorite radio stations
         * 
         * @private
         * @function
         * 
         * @param {int} frequency
         * 
         * @return {jQuery} An jQuery object, which contains the child 
         *                  jQuery objects, which will be deleted
         */
        removeFavorite = (function() {
            var stations = data.stations,
                $allStations = $("#allStations ul"),
                $favoriteStations = $("#favoriteStations ul");
            
            return function(frequency) {
                $.urc("remove", Urc.args2obj("favoriteStations", frequency, true));
                // DOM
                var $removedStations = $favoriteStations
                    .children("[data-frequency="+frequency+"]")
                        .fadeOut(transitionSpeed, function() {
                            $(this).remove();
                        });
                        
                $allStations
                    .children("[data-frequency="+frequency+"]")
                        .find(".ui-button span")
                            .text("add");
                        
                highlightList();
                findAndUpdateCurrentStation(data.frequencies[data.frequencyBand].frequencyBandColumn);
                
                // update stations index
                var bandStations = stations[getBand(frequency)];
                bandStations.splice(bandStations.indexOf(frequency), 1);
                
                return $removedStations;
            }; 
        }()),
        /**
         * Highlight the container, if a favorite radio station was added or
         * deleted
         * 
         * @private
         * @static
         * @function
         * 
         * @return {jQuery} The jQuery object that is highlighted
         */
        highlightList = (function() {
            var $ul = $("#list .tabs > ul");
            
            return function() {
                return $ul.animate({
                    backgroundColor: "#c4c4c4"//"#1ebc4b"
                }, transitionSpeed, function() {
                    $ul.animate({
                        backgroundColor: "#1e1c1d"
                    }, transitionSpeed);
                });
            };
        }()),
        /**
         * Get the frequency in a user-friendly readable format 
         * 
         * @private
         * @static
         * @function
         * 
         * @param {int} frequency
         * @param {boolean} [withoutUnit=false]
         * 
         * @return {string} The frequency in a user-friendly format 
         */
        getFormatedFrequency = function(frequency, withoutUnit) {
            var band = getBand(frequency);
            
            switch(band) {
                case "LF": // same as MF
                case "MF": frequency = (frequency/1000).toFixed(2); break;
                case "HF": frequency = (frequency/1000000).toFixed(2); break;
            }
            if(withoutUnit) {
                return frequency;
            } else {
                return band == "HF" ? frequency + " MHz" : frequency + " KHz";
            }
        },
        /**
         * Get the frequency band for a given frequency
         * 
         * @private
         * @static
         * 
         * @param {int} frequency
         * 
         * @return {String} The band of the frequency
         */
        getBand = function(frequency) {
            var ranges = data.frequencyBandRanges;
            
            if(frequency > ranges.HF.bandMin) {
                return "HF";
            } else if(frequency > ranges.MF.bandMin) {
                return "MF";
            } else {
                return "LF";
            }
        };
    
   /**
    * See the jQuery Library ({@link http://api.jquery.com}) for full details.
    * 
    * @name jQuery
    * @constructor
    * @class
    * See the jQuery Library ({@link http://api.jquery.com}) for full details. 
    * This just documents the function and classes that are added to jQuery 
    * by this plugin.
    * @see {@link http://api.jquery.com}
    */
    
    /**
     * use the jQuery URC Plugin to control the radio
     * 
     * @name jQuery.urc
     * @see <a href='../jqueryurc/docu/index.html'>jQuery URC Plugin Documentation</a>
     */
    $.urc("http://hdm-stuttgart.de/urclab2012/radio/socket.uis", {
        mapping: {
            isOn: "boolean",
            isMuted: "boolean",
            isUpdating: "boolean",
            volume: "int",
            treble: "int",
            bass: "int",
            balance: "int",
            signalStrength: "int",
            sleepTimerIsActive: "boolean",
            reminderIsActive: "boolean",
            "frequencyBandRanges.bandMax": "int",
            "frequencyBandRanges.bandMin": "int",
            "frequencies.frequencyBandColumn": "int"
        },
        updateInterval: 5000,
        onUpdate: function(event, path, value) {
            set(false, path, value);
        },
        onInit: function() {
            // activate simulator
            simulator.activate();
            
            var o = {};
            o[data.frequencyBand] = data.frequencies[data.frequencyBand];
            setter.frequencies(o, true);
            
            $("#allStations li").each(function() {
                var $this = $(this);
                var frequency = parseInt($this.attr("data-frequency"), 10);
                
                if(data.favoriteStations[frequency] === undefined) {
                    $this.find(".ui-button-text").text("add");
                } else {
                    $this.find(".ui-button-text").text("remove");
                }
            });
            
            $("#loaderScreen").children(".text")
                    .html("All data loaded<br />Have fun with the radio!")
                    .end()
                .children(".loader")
                    .remove();

            setTimeout(function() {
                $("#loaderScreen").fadeOut(1500, function() {
                    $(this).remove();
                });
            }, 1500);
        },
        data: data,
        verbose: true,
        bundles: {
            favoriteStations: ["favoriteStationsBand", "favoriteStationsNameOfStation"],
            frequencyBandRanges: ["bandMin", "bandMax"]
        },
        setterDelay: 250
    });
    
    
});
